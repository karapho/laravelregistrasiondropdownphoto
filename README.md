# Welcome to #LaravelRegistrasionDropdownPhoto!

Pertama Clone dahulu Repo ini

    git clone git@gitlab.com:karapho/laravelregistrasiondropdownphoto.git
    
Lalu masuk ke folder nya

Buka terminal, lalu jalankan perintah 

    composer install

Selanjutnya jalankan perintah 

    npm install

dan

    npm run dev

Buat file .env dengan perintah

    cp .env.example .env

Lalu, Jalankan perintah

    php artisan key:generate


lalu atur koneksi ke database pada file .env yang telah terbuat sebelumnya

Untuk dummy datanya jalankan perintah berikut

    php artisan migrate --seed
