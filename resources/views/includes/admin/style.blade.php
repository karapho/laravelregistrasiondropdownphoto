<link rel="icon" href="{{asset('storage/assets/admin/img/icon.ico')}}" type="image/x-icon" />

<!-- Fonts and icons -->
<script src="{{asset('storage/assets/admin/js/plugin/webfont/webfont.min.js')}}"></script>
<script>
    WebFont.load({
        google: {"families":["Lato:300,400,700,900"]},
        custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ["{{asset('storage/assets/admin/css/fonts.min.css')}}"]},
        active: function() {
            sessionStorage.fonts = true;
        }
    });
</script>

<!-- CSS Files -->
<link rel="stylesheet" href="{{asset('storage/assets/admin/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('storage/assets/admin/css/atlantis.min.css')}}">
