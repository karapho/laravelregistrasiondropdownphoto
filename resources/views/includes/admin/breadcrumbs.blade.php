@unless ($breadcrumbs->isEmpty())
{{-- {{dd($breadcrumbs)}} --}}
@foreach ($breadcrumbs as $item)
@if ($loop->last)
<h4 class="page-title">{{Str::of($item->title)->words(3)}}</h4>
@endif
@endforeach
<ul class="breadcrumbs">
    @foreach ($breadcrumbs as $breadcrumb)

    @if (!is_null($breadcrumb->url) && !$loop->last)
    <li class="nav-home"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
    <li class="separator">
        <i class="flaticon-right-arrow"></i>
    </li>
    @else
    <li class="nav-home active"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
    @endif

    @endforeach
</ul>
@endunless