<!--   Core JS Files   -->
<script src="{{asset('storage/assets/admin/js/core/jquery.3.2.1.min.js')}}"></script>
<script src="{{asset('storage/assets/admin/js/core/popper.min.js')}}"></script>
<script src="{{asset('storage/assets/admin/js/core/bootstrap.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{asset('storage/assets/admin/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
<script src="{{asset('storage/assets/admin/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

<!-- jQuery Scrollbar -->
<script src="{{asset('storage/assets/admin/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>


<!-- Chart JS -->
<script src="{{asset('storage/assets/admin/js/plugin/chart.js/chart.min.js')}}"></script>

<!-- jQuery Sparkline -->
<script src="{{asset('storage/assets/admin/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Chart Circle -->
<script src="{{asset('storage/assets/admin/js/plugin/chart-circle/circles.min.js')}}"></script>

<!-- Datatables -->
<script src="{{asset('storage/assets/admin/js/plugin/datatables/datatables.min.js')}}"></script>

<!-- Bootstrap Notify -->
<script src="{{asset('storage/assets/admin/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

<!-- jQuery Vector Maps -->
<script src="{{asset('storage/assets/admin/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('storage/assets/admin/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

<!-- Sweet Alert -->
<script src="{{asset('storage/assets/admin/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

<!-- Atlantis JS -->
<script src="{{asset('storage/assets/admin/js/atlantis.min.js')}}"></script>

{{-- <!-- Atlantis DEMO methods, don't include it in your project! --> --}}
{{-- <script src="{{asset('storage/assets/admin/js/setting-demo.js')}}"></script>
<script src="{{asset('storage/assets/admin/js/demo.js')}}"></script> --}}
<script>
    $(document).on("click", ".delete", function(e){
        console.log('test');
        event.preventDefault();
        var action = $(this);
        
        swal({
            title: 'Hapus data ini ?',
            text: "Aksi tidak dapat di kembalikan!",
            type: 'warning',
            buttons:{
                cancel: {
                    visible: true,
                    text : 'Batalkan!',
                    className: 'btn btn-danger'
                },        			
                confirm: {
                    text : 'Yes, Hapus!',
                    className : 'btn btn-success'
                }
            }
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'DELETE',
                    url: action.data("remote"),
                    // data: {_token:_token,kls:_kls,tgl:_tgl,jmm:_jmm,jms:_jms,dsn:_dsn,sbs:_sbs,sks:_sks,rng:_rng,kd_mk:_idmk},
                    data:{
                        "_token": "{{ csrf_token() }}",
                    },
                    beforeSend: function () {
                        // $('.preloader').toggle();
                        // $('.preloader').css("background-color", "#0000001f");
                    },
                    error: function (request, html, error) {
                        // $('.preloader').toggle();
                    },
                    success: function(response) {
                        // $('.preloader').toggle();
                        swal("Success!", "Data telah terhapus!", {
								icon: "success",
								buttons : {
									confirm : {
										className: 'btn btn-success'
									}
								}
							});
                        setTimeout(location.reload.bind(location), 1350);
                        // alert(response.pesan);
                    }
                });
            }
        })
    });
   

    // var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');

    // var mytotalIncomeChart = new Chart(totalIncomeChart, {
    //     type: 'bar',
    //     data: {
    //         labels: ["S", "M", "T", "W", "T", "F", "S", "S", "M", "T"],
    //         datasets : [{
    //             label: "Total Income",
    //             backgroundColor: '#ff9e27',
    //             borderColor: 'rgb(23, 125, 255)',
    //             data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10],
    //         }],
    //     },
    //     options: {
    //         responsive: true,
    //         maintainAspectRatio: false,
    //         legend: {
    //             display: false,
    //         },
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     display: false //this will remove only the label
    //                 },
    //                 gridLines : {
    //                     drawBorder: false,
    //                     display : false
    //                 }
    //             }],
    //             xAxes : [ {
    //                 gridLines : {
    //                     drawBorder: false,
    //                     display : false
    //                 }
    //             }]
    //         },
    //     }
    // });

    // $('#lineChart').sparkline([105,103,123,100,95,105,115], {
    //     type: 'line',
    //     height: '70',
    //     width: '100%',
    //     lineWidth: '2',
    //     lineColor: '#ffa534',
    //     fillColor: 'rgba(255, 165, 52, .14)'
    // });
</script>