        <!-- Sidebar -->
        <div class="sidebar sidebar-style-2">
            <div class="sidebar-wrapper scrollbar scrollbar-inner">
                <div class="sidebar-content">
                    <div class="user">
                        <div class="avatar-sm float-left mr-2">
                            <img src="{{asset('storage/assets/admin/img/logo-pal.png')}}" alt="..." class="avatar-img rounded-circle">
                        </div>
                        <div class="info">
                            <a data-toggle="collapse" href="#collapseExample3" aria-expanded="true">
                                <span>
                                    Admin
                                    <span class="user-level">Administrator</span>
                                    <span class="caret"></span>
                                </span>
                            </a>
                            <div class="clearfix"></div>

                            <div class="collapse in" id="collapseExample">
                                <ul class="nav">
                                    <li>
                                        <a href="#profile">
                                            <span class="link-collapse">My Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#edit">
                                            <span class="link-collapse">Edit Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#settings">
                                            <span class="link-collapse">Settings</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-primary">
                        <li class="nav-item {{(request()->is('dashboard*')) ? 'active' : ''}}">
                            <a data-toggle="collapse" href="#utama">
                                <i class="fas fa-layer-group"></i>
                                <p>Menu Utama</p>
                                <span class="caret"></span>
                            </a>
                            <div class="collapse" id="utama">
                                <ul class="nav nav-collapse">
                                    <li class="{{(request()->is('dashboard*')) ? 'active' : ''}}">
                                        <a href="#">
                                            <span class="sub-item">Dashboard</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </li>
                        <li class="nav-section">
                            <span class="sidebar-mini-icon">
                                <i class="fa fa-ellipsis-h"></i>
                            </span>
                            <h4 class="text-section">Bagan Pertama</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Sidebar -->