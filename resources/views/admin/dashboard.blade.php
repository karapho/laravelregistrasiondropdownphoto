@extends('layouts.admindashboard')

@section('title', 'Dashboard')

@section('modulname','Dashboard')
{{-- {{ dd(get_defined_vars()) }} --}}
{{-- @foreach ($data as $key => $value)
    {{$key}}
@endforeach --}}
@section('content')
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    <div class="card-title pb-3">Overall statistics</div>
                    {{-- <div class="card-category pb-3">Daily information about statistics in system</div> --}}
                    {{-- <div class="row">
                        @foreach (array_chunk($data, 4, true) as $chunk)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title"></div>
                                </div>
                                <div class="card-body pb-0">
                                    @foreach ($chunk as $key=>$value)
                                    <div class="d-flex">
                                        <div class="avatar">
                                            <i class="fas fa-file"></i>
                                        </div>
                                        <div class="flex-1 pt-1 ml-2">
                                            <h4 class="fw-bold mb-1">{{$key}}</h4>
                                        </div>
                                        <div class="d-flex ml-auto align-items-center">
                                            <h3 class="text-info fw-bold">{{$value}}</h3>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div> --}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- 
<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <div class="card-title"></div>
        </div>
        <div class="card-body pb-0">
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..."
                        class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][0]}}</h4>
</div>
<div class="d-flex ml-auto align-items-center">
    <h3 class="text-info fw-bold">{{$data['value'][0]}}</h3>
</div>
</div>
<div class="separator-dashed"></div>
<div class="d-flex">
    <div class="avatar">
        <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
    </div>
    <div class="flex-1 pt-1 ml-2">
        <h4 class="fw-bold mb-1">{{$data['key'][1]}}</h4>
    </div>
    <div class="d-flex ml-auto align-items-center">
        <h3 class="text-info fw-bold">{{$data['value'][1]}}</h3>
    </div>
</div>
<div class="separator-dashed"></div>
<div class="d-flex">
    <div class="avatar">
        <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
    </div>
    <div class="flex-1 pt-1 ml-2">
        <h4 class="fw-bold mb-1">{{$data['key'][2]}}</h4>
    </div>
    <div class="d-flex ml-auto align-items-center">
        <h3 class="text-info fw-bold">{{$data['value'][2]}}</h3>
    </div>
</div>
<div class="separator-dashed"></div>
<div class="d-flex">
    <div class="avatar">
        <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
    </div>
    <div class="flex-1 pt-1 ml-2">
        <h4 class="fw-bold mb-1">{{$data['key'][3]}}</h4>
    </div>
    <div class="d-flex ml-auto align-items-center">
        <h3 class="text-info fw-bold">{{$data['value'][3]}}</h3>
    </div>
</div>
</div>
</div>
</div>
<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <div class="card-title"></div>
        </div>
        <div class="card-body pb-0">
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][4]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][4]}}</h3>
                </div>
            </div>
            <div class="separator-dashed"></div>
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][5]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][5]}}</h3>
                </div>
            </div>
            <div class="separator-dashed"></div>
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][6]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][6]}}</h3>
                </div>
            </div>
            <div class="separator-dashed"></div>
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][7]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][7]}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <div class="card-title"></div>
        </div>
        <div class="card-body pb-0">
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][8]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][8]}}</h3>
                </div>
            </div>
            <div class="separator-dashed"></div>
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][9]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][9]}}</h3>
                </div>
            </div>
            <div class="separator-dashed"></div>
            <div class="d-flex">
                <div class="avatar">
                    <img src="../assets/img/logoproduct.svg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="flex-1 pt-1 ml-2">
                    <h4 class="fw-bold mb-1">{{$data['key'][10]}}</h4>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{$data['value'][10]}}</h3>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@push('addon-script')
<script>

</script>
@endpush