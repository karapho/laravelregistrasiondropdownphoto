<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>LaraveCrud - @yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    @stack('prepend-style')
    @include('includes.admin.style')
    @stack('addon-style')
</head>

<body>
    <div class="wrapper">
        @include('includes.admin.navbar')

        @include('includes.admin.sidebar')

        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        @yield('breadcrumbs')
                    </div>
                    @yield('content')

                </div>
            </div>
            @include('includes.admin.footer')

        </div>
        @stack('prepend-script')

        @include('includes.admin.script')

        @stack('addon-script')

</body>

</html>