<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>LaraCrud - @yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    @stack('prepend-style')
    @include('includes.admin.style')
    @stack('addon-style')
</head>

<body>
    <div class="wrapper">
        @include('includes.admin.navbar')

        @include('includes.admin.sidebar')

        <div class="main-panel">
            <div class="content">
                <div class="panel-header bg-primary-gradient">
                    <div class="page-inner py-5">
                        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                            <div>
                                <h2 class="text-white pb-2 fw-bold">@yield('modulname', 'Default Modul Name')</h2>
                                <!-- <h5 class="text-white op-7 mb-2">Free Bootstrap 4 Admin Dashboard</h5> -->
                            </div>
                            <!-- <div class="ml-md-auto py-2 py-md-0">
								<a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
								<a href="#" class="btn btn-secondary btn-round">Add Customer</a>
							</div> -->
                        </div>
                    </div>
                </div>
                @yield('content')
                
            </div>
            @include('includes.admin.footer')
        </div>

    </div>
    @stack('prepend-script')

    @include('includes.admin.script')

    @stack('addon-script')

</body>

</html>